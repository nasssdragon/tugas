<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@utama");
Route::get('/register', "AuthController@register");
Route::post('/welcome', "AuthController@welcome");

Route::get('/data-tables', function(){
    return view('page.data-tables');
});

Route::get('/table', function(){
    return view('page.table');
});

//CRUD Cast

//Create
//Route untuk menuju form pembuatan data pemain film baru
Route::get('/cast/create', 'CastController@create');
//Menyimpan data ke database
Route::post('/cast', 'CastController@store');

//Read
//Route untuk menampilkan semua cast di database
Route::get('/cast', 'CastController@show');
//Route untuk detail cast berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@index');

//Update
//Route untuk mengarah ke halaman form edit cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk update data berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');


