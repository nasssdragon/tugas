@extends('layout.master')
@section('title')
Halaman Edit Cast Pemain Film
@endsection
   
@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" class="form-control" value="{{$cast-> nama}}" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur Cast</label>
      <input type="text" class="form-control" value="{{$cast-> umur}}" name="umur"> 
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
       <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast-> bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection