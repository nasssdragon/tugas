@extends('layout.master')
@section('title')
Halaman  Cast Pemain Film
@endsection
   
@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Cast</th>
        <th scope="col">Umur Cast</th>
        <th scope="col">Bio Cast</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
           <tr>
                <th>{{$key+1}}</th>
                <td>{{$item-> nama}}</td>
                <td>{{$item-> umur}}</td>
                <td>{{$item-> bio}}</td>
                <td>
                    <form action="/cast/{{$item-> id}}" method="post">
                    <a href="/cast/{{$item-> id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item-> id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
             <tr>
                <td>Data Cast masih kosong</td>
             </tr>   
        @endforelse
    </tbody>
  </table>
@endsection