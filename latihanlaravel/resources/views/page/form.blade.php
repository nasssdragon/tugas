@extends('layout.master')
@section('title')
Buat Account Baru
@endsection
   
@section('content')
    
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
    @csrf
    <label>First Name</label> <br>
    <input type="text" name="fname"> <br> <br>
    <label>Last Name</label> <br>
    <input type="text" name="lname"> <br> <br>
    <label>Gender</label> <br> <br>
    <input type="radio" name="gender" value="01">Male<br>
    <input type="radio" name="gender" value="02">Female<br>
    <input type="radio" name="gender" value="03">Others<br> <br>
    <label>Nationality:</label> 
    <select name="">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapore">Singapore</option>
        <option value="Australia">Australia</option>
    </select> <br> <br>
    <label>Language Spoken</label> <br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
    <input type="checkbox" name="Language Spoken">English<br>
    <input type="checkbox" name="Language Spoken">Other<br> <br>
    <label>Bio</label> <br> <br>
    <textarea name="message" rows="18" cols="30"></textarea> <br> <br>


    <input type="submit" value="Sign Up">
</form>
@endsection       
   
